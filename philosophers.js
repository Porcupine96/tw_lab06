// Teoria Współbieżnośi, implementacja problemu 5 filozofów w node.js
// Opis problemu: http://en.wikipedia.org/wiki/Dining_philosophers_problem
// 1. Dokończ implementację funkcji podnoszenia widelca (Fork.acquire).
// 2. Zaimplementuj "naiwny" algorytm (każdy filozof podnosi najpierw lewy, potem
//    prawy widelec, itd.).
// 3. Zaimplementuj rozwiązanie asymetryczne: filozofowie z nieparzystym numerem
//    najpierw podnoszą widelec lewy, z parzystym -- prawy.
// 4. Zaimplementuj rozwiązanie z kelnerem (opisane jako "Arbitrator solution"
//    w wikipedii).
// 5. Zaimplementuj rozwiążanie z jednoczesnym podnoszeniem widelców:
//    filozof albo podnosi jednocześnie oba widelce, albo żadnego.
// 6. Uruchom eksperymenty dla różnej liczby filozofów i dla każdego wariantu
//    implementacji zmierz średni czas oczekiwania każdego filozofa na dostęp
//    do widelców. Wyniki przedstaw na wykresach.

const _ = require('lodash');
const async = require('async');
const fs = require('fs');

const FREE = 0;
const TAKEN = 1;

const Fork = function() {
    this.state = FREE;
    return this;
}

Fork.prototype.acquire = function(cb) {
    let numberOfTries = 0;

    const waitForFork = timeToWait => {
        setTimeout(t => {
            if (this.state === FREE) {
                this.state = TAKEN;
                if (cb) cb();
            } else {
                numberOfTries++;
                const nextWait = Math.floor(Math.random() * Math.pow(2, numberOfTries));
                waitForFork(nextWait)
            }
        }, timeToWait)
    }

    waitForFork(1)
}

Fork.prototype.acquireForce = function (cb) {
  this.state = 1;
  if(cb) cb();
}

Fork.prototype.release = function(cb) {
    this.state = FREE;
    if(cb) cb();
}

var Philosopher = function(id, forks) {
    this.id = id;
    this.forks = forks;
    this.f1 = id % forks.length;
    this.f2 = (id+1) % forks.length;
    return this;
}

Philosopher.prototype.startNaive = function(count) {
    const [forks, f1, f2] = [this.forks, this.f1, this.f2];
    simulate(this, forks[f1], forks[f2], count);
}

Philosopher.prototype.startAsym = function(count) {
    const [id, forks, f1, f2] = [this.id, this.forks, this.f1, this.f2];
    const left = (id % 2 === 0) ? forks[f1] : forks[f2];
    const right = (id % 2 === 0) ? forks[f2] : forks[f1];
    simulate(this, left, right, count);
}

Philosopher.prototype.eat = function(callback) {
    console.log(this.id, 'eating');
    callback()
}

Philosopher.prototype.finishEating = function finish_eating(callback) {
    console.log(this.id, 'finished_eating');
    callback()
}

function simulate(philosopher, leftFork, rightFork, count) { 
    if (count === 0)  {
        console.log(philosopher.id, 'done');
        return;
    }

    async.waterfall(
        [
            cb => leftFork.acquire(cb),
            cb => rightFork.acquire(cb),
            cb => philosopher.eat(cb), 
            cb => leftFork.release(cb),
            cb => rightFork.release(cb),
            cb => philosopher.finishEating(cb)
        ], 
        err => {
            if (err) console.error('Error in startNaive', errror);
            else simulate(philosopher, leftFork, rightFork, count - 1)
        }
    )
}

const SERVING = 1;

const Waiter = function() {
    this.state = FREE;
    return this;
}

Waiter.prototype.askForFork = function(callback) {
    if (this.state === SERVING) {
        setTimeout(cb => this.askForFork(callback), 1);
    } else {
        this.state = SERVING;
        if (callback) callback();
    }
}

Waiter.prototype.releaseFromServing = function(callback) {
    this.state = FREE;
    callback();
}

Philosopher.prototype.startConductor = function(count, waiter) {
    const [forks, f1, f2] = [this.forks, this.f1, this.f2];
    const [leftFork, rightFork] = [forks[f1], forks[f2]];

    if (count === 0)  {
        console.log(this.id, 'done');
        return;
    }

    async.waterfall(
        [
            cb => waiter.askForFork(cb),
            cb => leftFork.acquire(cb),
            cb => rightFork.acquire(cb),
            cb => waiter.releaseFromServing(cb),
            cb => this.eat(cb), 
            cb => leftFork.release(cb),
            cb => rightFork.release(cb),
            cb => this.finishEating(cb)
        ], 
        err => {
            if (err) console.error('Error in startConductor', errror);
            else this.startConductor(count - 1, waiter);
        }
    )
}

function acquireBoth(leftFork, rightFork, cb) {
    const waitForBoth = timeToWait => {
        setTimeout(t => {
            if (leftFork.state === FREE && rightFork.state === FREE) {
                leftFork.state = TAKEN;
                rightFork.state = TAKEN;
                if (cb) cb();
            } else {
                numberOfTries++;
                const nextWait = Math.floor(Math.random() * Math.pow(2, numberOfTries));
                waitForBoth(nextWait)
            }
        }, timeToWait)
    }
    waitForBoth(1)
}

function releaseBoth(leftFork, rightFork, cb) {
    leftFork.state = FREE;
    rightFork.state = FREE;
    if (cb) cb();
}

Philosopher.prototype.startBothForksTogether = function(count) {
    const [forks, f1, f2] = [this.forks, this.f1, this.f2];
    const [leftFork, rightFork] = [forks[f1], forks[f2]];

    if (count === 0)  {
        console.log(this.id, 'done');
        return;
    }

    async.waterfall(
        [
            cb => acquireBoth(leftFork, rightFork, cb),
            cb => this.eat(cb), 
            cb => releaseBoth(leftFork, rightFork, cb),
            cb => this.finishEating(cb)
        ], 
        err => {
            if (err) console.error('Error in startBothForksTogeher', errror);
            else this.startBothForksTogether(count - 1);
        }
    )
}


var N = 5;
var forks = [];
var philosophers = []

for (var i = 0; i < N; i++) {
    forks.push(new Fork());
}

const conductor = new Fork();
const waiter = new Waiter();

for (var i = 0; i < N; i++) {
    philosophers.push(new Philosopher(i, forks));
}

for (var i = 0; i < N; i++) {
//    philosophers[i].startNaive(10);
//    philosophers[i].startAsym(10);
//    philosophers[i].startConductor(10, waiter);
   philosophers[i].startBothForksTogether(10);
};